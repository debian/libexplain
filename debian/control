Source: libexplain
Section: devel
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: dpkg-dev (>= 1.22.5),
 bison,
 debhelper-compat (= 13),
 ghostscript,
 groff,
 libacl1-dev,
 libcap-dev [linux-any],
 libtool-bin,
 linux-libc-dev (>= 4.10) [linux-any],
 lsof [linux-any],
 netbase
Rules-Requires-Root: no
Standards-Version: 4.7.0
Homepage: http://libexplain.sourceforge.net/
Vcs-Browser: https://salsa.debian.org/debian/libexplain
Vcs-Git: https://salsa.debian.org/debian/libexplain.git

Package: explain
Architecture: linux-any
Description: utility to explain system call errors
 This package provides an explain(1) command to explain Unix and Linux system
 call errors, after the fact.
Depends: lsof, ${misc:Depends}, ${shlibs:Depends}

Package: libexplain-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Description: library of system-call-specific strerror repl - documentation
 This package provides a library which may be used to explain Unix and Linux
 system call errors.  The library is not quite a drop-in replacement for
 strerror, but it comes close, with each system call having a dedicated
 libexplain function.
 .
 This package contains the documentation.
Depends: ${misc:Depends}

Package: libexplain51t64
Provides: ${t64:Provides}
Replaces: libexplain51
Breaks: libexplain51 (<< ${source:Version})
Section: libs
Architecture: linux-any
Multi-Arch: same
Description: library of system-call-specific strerror repl
 This package provides a library which may be used to explain Unix and Linux
 system call errors.  The library is not quite a drop-in replacement for
 strerror, but it comes close, with each system call having a dedicated
 libexplain function.
Depends: lsof, ${misc:Depends}, ${shlibs:Depends}

Package: libexplain-dev
Section: libdevel
Architecture: linux-any
Breaks: explain (<< 1.0)
Replaces: explain (<< 1.0)
Description: library of system-call-specific strerror repl - development files
 This package provides a library which may be used to explain Unix and Linux
 system call errors.  The library is not quite a drop-in replacement for
 strerror, but it comes close, with each system call having a dedicated
 libexplain function.
 .
 This package contains the development files.
Depends:
 libacl1-dev,
 libexplain51t64 (= ${binary:Version}),
 lsof,
 ${misc:Depends},
 ${shlibs:Depends}
